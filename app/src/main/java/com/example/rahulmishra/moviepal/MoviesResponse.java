package com.example.rahulmishra.moviepal;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rahul Mishra on 20-07-2017.
 */

class MoviesResponse {

    @SerializedName("results")
    private List<Movie> results;

    public List<Movie> getResults() {
        return results;
    }
}
