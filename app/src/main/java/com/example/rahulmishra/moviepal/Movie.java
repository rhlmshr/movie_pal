package com.example.rahulmishra.moviepal;

import android.util.Log;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Rahul Mishra on 20-07-2017.
 */

public class Movie {

    private int id ;
    private String title ;
    private String poster_path ;
    private String backdrop_path ;
    private String overview ;
    private String release_date ;
    private final String image_path = "http://image.tmdb.org/t/p/w500/" ;

    public Movie(int id, String title, String poster_path, String backdrop_path, String overview, String release_date) {
        this.id = id;
        this.title = title;
        this.poster_path = poster_path;
        this.backdrop_path = backdrop_path;
        this.overview = overview;
        this.release_date = release_date;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPoster_path() {
        return image_path + poster_path;
    }

    public String getBackdrop_path() {
        return image_path + backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public long getRelease_date() throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(release_date).getTime();
    }
}