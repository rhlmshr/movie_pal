package com.example.rahulmishra.moviepal;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Date;

/**
 * Created by Rahul Mishra on 20-07-2017.
 */

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder>  {

    private List<Movie> movies ;
    private Context context ;
    private int listLayout ;

    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        ImageView backdrop ;
        RoundedImageView img_cover ;
        TextView movie_name ;
        TextView movie_plot ;
        TextView movie_release_date ;
        View backView ;

        public MovieViewHolder(View view) {
            super(view) ;
            movie_name = view.findViewById(R.id.txt_movie_details) ;
            movie_plot = view.findViewById(R.id.txt_plot_d) ;
            movie_release_date = view.findViewById(R.id.txt_release_d) ;
            img_cover = view.findViewById(R.id.img_cover_d) ;
            backdrop = view.findViewById(R.id.img_background) ;
            backView = view.findViewById(R.id.vw_blacklayer) ;
        }
    }

    public MoviesAdapter(List<Movie> movies, int listLayout, Context context) {
        this.movies = movies;
        this.context = context;
        this.listLayout = listLayout ;
    }

    @Override
    public MoviesAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(listLayout, parent, false);
        return new MovieViewHolder(view) ;
    }

    @Override
    public void onBindViewHolder(MoviesAdapter.MovieViewHolder holder, int position) {
        holder.movie_name.setText(movies.get(position).getTitle()) ;
        holder.movie_plot.setText(movies.get(position).getOverview()) ;
        try {
            holder.movie_release_date.setText(DateFormat.getDateInstance()
                    .format(new Date(movies.get(position).getRelease_date()))) ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Picasso.with(context).load(movies.get(position).getPoster_path()).into(holder.img_cover) ;
        Picasso.with(context).load(movies.get(position).getBackdrop_path()).into(holder.backdrop) ;
        ObjectAnimator fade = ObjectAnimator.ofFloat(holder.backView, View.ALPHA, 1f,.3f);
        fade.setDuration(1500);
        fade.setInterpolator(new LinearInterpolator());
        fade.start();
    }

    @Override
    public int getItemCount() {
        return movies.size() ;
    }
}
