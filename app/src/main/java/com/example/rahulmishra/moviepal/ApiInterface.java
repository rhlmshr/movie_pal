package com.example.rahulmishra.moviepal;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Rahul Mishra on 20-07-2017.
 */

public interface ApiInterface {

    @GET("movie/upcoming/")
    Call<MoviesResponse> getUpcomingMovies(@Query("api_key") String apiKey) ;


}
